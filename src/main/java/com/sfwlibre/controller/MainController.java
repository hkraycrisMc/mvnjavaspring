package com.sfwlibre.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.sfwlibre.service.UsuarioService;

@Controller
@RequestMapping("/")
public class MainController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping( method = RequestMethod.GET)
	public String main(ModelMap modelMap){
		modelMap.addAttribute("listaUsuarios", usuarioService.obtenerUsuarios());
		return "index";
	}
	
   
	@RequestMapping(value="/app/listUser", method= RequestMethod.GET)
	public String listUser(ModelMap modelMap){
		modelMap.addAttribute("listaUsuarios", usuarioService.obtenerUsuarios());
		return "listUser";
	}
	
	
}
