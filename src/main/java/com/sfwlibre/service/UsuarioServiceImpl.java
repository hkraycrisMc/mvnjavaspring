package com.sfwlibre.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfwlibre.dao.UsuarioDAO;
import com.sfwlibre.dto.UsuarioDTO;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioDAO usuarioDAO;
	
	public List<UsuarioDTO> obtenerUsuarios() {
		return usuarioDAO.obtenerUsuarios();
	}

}
