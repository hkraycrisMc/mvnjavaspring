package com.sfwlibre.service;

import java.util.List;

import com.sfwlibre.dto.UsuarioDTO;

public interface UsuarioService {

	List<UsuarioDTO> obtenerUsuarios();
}
