package com.sfwlibre.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sfwlibre.dto.UsuarioDTO;

@Repository
public class UsuarioDAOImpl implements UsuarioDAO{

	public List<UsuarioDTO> obtenerUsuarios(){
		List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();
		UsuarioDTO usuario1 = new UsuarioDTO();
		
		usuario1.setNombre("Beto");
		usuario1.setApellidoP("Cuevas");
		usuario1.setApellidoM("Arrollo");
		usuario1.setEmail("beto.cuevas@gmail.com");
		usuario1.setContrasenia("lojij23n4o2349");
		listaUsuarios.add(usuario1);
        UsuarioDTO usuario2 = new UsuarioDTO();
        usuario2.setNombre("Beto");
		usuario2.setApellidoP("Martinez");
		usuario2.setApellidoM("Arrollo");
		usuario2.setEmail("beto.martinez@gmail.com");
		usuario2.setContrasenia("982347uf87y345");
		listaUsuarios.add(usuario2);
		
		return listaUsuarios;
	}


}
