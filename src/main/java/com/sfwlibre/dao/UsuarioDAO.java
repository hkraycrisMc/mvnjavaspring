package com.sfwlibre.dao;

import java.util.List;

import com.sfwlibre.dto.UsuarioDTO;

public interface UsuarioDAO {

	  List<UsuarioDTO> obtenerUsuarios();
}
